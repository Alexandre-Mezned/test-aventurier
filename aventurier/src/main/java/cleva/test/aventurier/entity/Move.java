package cleva.test.aventurier.entity;

import java.awt.*;

public class Move {
    Point initialPosition;
    Direction[] directions;

    public Move(Point initialPosition, Direction[] directions) {
        this.initialPosition = initialPosition;
        this.directions = directions;
    }

    public Point getInitialPosition() {
        return initialPosition;
    }

    public void setInitialPosition(Point initialPosition) {
        this.initialPosition = initialPosition;
    }

    public Direction[] getDirections() {
        return directions;
    }

    public void setDirections(Direction[] directions) {
        this.directions = directions;
    }
}
