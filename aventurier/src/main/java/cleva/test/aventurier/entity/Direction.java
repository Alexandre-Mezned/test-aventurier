package cleva.test.aventurier.entity;

public enum Direction {
    N,
    S,
    E,
    O
}
