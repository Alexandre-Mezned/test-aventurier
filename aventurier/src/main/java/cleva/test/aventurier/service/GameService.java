package cleva.test.aventurier.service;

import cleva.test.aventurier.entity.Direction;
import cleva.test.aventurier.entity.Move;
import cleva.test.aventurier.exception.GamePreconditionFailed;
import cleva.test.aventurier.exception.PositionNotAcceptableException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class GameService {

    public Character[][] map;

    /**
     * Imports the map file and inserts characters in array.
     * @return Characters 2-dimensional array containing the map data.
     * @throws IOException Throwed if the file is already in use, unreachable or unreadable.
     */
    public Character[][] loadMap() throws IOException {

        File resource = new ClassPathResource("carte.txt").getFile();

        final List<String> lines = Files.readAllLines(resource.toPath());

        final int height = lines.size();

        final Integer width = lines.stream()
                .max(Comparator.comparingInt(String::length))
                .map(String::length)
                .orElse(-1);

        this.map = new Character[height][width];

        for (int i = 0; i < lines.size(); i++) {

            final String line = lines.get(i);

            for (int j = 0; j < line.length(); j++) {
                final char c = line.charAt(j);
                this.map[i][j] = c;
            }
        }

        return this.map;
    }

    /**
     * Applies directions from an initial position.
     * @param move Initial position (Point) and the list of directions to be performed.
     * @return Initial, final and transitory positions.
     */
    public List<Point> applyMoves(Move move) {

        if(this.map == null) {
            throw new GamePreconditionFailed("Map is not loaded");
        }
        Point position =  move.getInitialPosition();
        Direction[] directions = move.getDirections();

        List<Point> positions = new ArrayList<>();

        checkPositionValidity(position);
        positions.add(position);

        for (Direction direction : directions) {
            position = applyDirection(position, direction);
            checkPositionValidity(position);
            positions.add(position);
        }

        return positions;
    }

    /**
     * Assigns a move to a position.
     * @param position Initial position (x,y).
     * @param direction One direction (N,S,E,O).
     * @return Assigned position.
     */
    private Point applyDirection(Point position, Direction direction) {

        int x = (int) position.getX();
        int y = (int) position.getY();

        switch(direction) {
            case N:
                y --;
                break;
            case S:
                y ++;
                break;
            case E:
                x ++;
                break;
            case O:
                x --;
                break;
        }

        return new Point(x,y);
    }

    /**
     * Checks the validation rules of the map cell.
     * @param position Position referencing a map cell.
     */
    private void checkPositionValidity(Point position) {

        boolean isEscape = map[(int) position.getY()][(int) position.getX()] == ' ';
        boolean isInXBound = (int) position.getX() >= 0 || (int) position.getX() < map[0].length;
        boolean isInYBound = (int) position.getY() >= 0 || (int) position.getY() < map.length;

        if(!(isEscape && isInXBound && isInYBound)) {
            throw new PositionNotAcceptableException("Position not valid : [" + (int) position.getX() + "," + (int) position.getY() + "]");
        }
    }

}
