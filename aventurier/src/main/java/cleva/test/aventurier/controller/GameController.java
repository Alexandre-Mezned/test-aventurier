package cleva.test.aventurier.controller;

import cleva.test.aventurier.entity.Move;
import cleva.test.aventurier.exception.PositionNotAcceptableException;
import cleva.test.aventurier.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.io.IOException;
import java.util.List;

@RestController
public class GameController {

    @Autowired
    private GameService gameService;

    @CrossOrigin
    @GetMapping("/")
    public Character[][] load() throws IOException {
        return gameService.loadMap();
    }

    @CrossOrigin
    @PutMapping("/move/apply")
    public List<Point> applyMoves(@RequestBody Move move) {
        return gameService.applyMoves(move);
    }
}
