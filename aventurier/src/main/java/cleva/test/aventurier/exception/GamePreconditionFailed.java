package cleva.test.aventurier.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class GamePreconditionFailed extends RuntimeException {

    public GamePreconditionFailed(String message) {
        super(message);
    }
}
