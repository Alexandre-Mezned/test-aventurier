package cleva.test.aventurier.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class PositionNotAcceptableException extends RuntimeException {

    public PositionNotAcceptableException(String message) {
        super(message);
    }
}
