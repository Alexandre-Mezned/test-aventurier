package cleva.test.aventurier.service;

import cleva.test.aventurier.entity.Direction;
import cleva.test.aventurier.entity.Move;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static cleva.test.aventurier.entity.Direction.*;
import static org.junit.jupiter.api.Assertions.*;

class GameServiceTest {

    GameService gameService = new GameService();

    @Test
    void loadMap() throws IOException {

        Character[][] expectedMap = {
                { '#', '#', '#', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', '#', '#', '#' },
                { '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '#' },
                { '#', '#', ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ', '#', '#' },
                { '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
                { '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#' },
                { '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#' },
                { '#', '#', '#', '#', '#', '#', ' ', '#', '#', ' ', ' ', '#', '#', ' ', ' ', '#', '#', '#', '#', '#' },
                { ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ' },
                { ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
                { ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ' },
                { ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ' },
                { ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
                { ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', '#', '#' },
                { '#', '#', '#', '#', '#', '#', ' ', '#', '#', ' ', ' ', '#', '#', ' ', '#', '#', '#', '#', '#', '#' },
                { '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#' },
                { '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#' },
                { '#', ' ', ' ', ' ', '#', '#', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', '#', '#', ' ', ' ', ' ', '#' },
                { '#', '#', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#', ' ', ' ', ' ', '#', '#' },
                { '#', '#', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', '#', '#' },
                { '#', '#', '#', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', '#', '#', '#' }
        };

        Character[][] actualMap = gameService.loadMap();

        assertArrayEquals(expectedMap, actualMap);
    }

    @Test
    void applyMoves() throws IOException {

        gameService.loadMap();

        Point initialPosition = new Point(3, 0);
        Direction[] directions = new Direction[] {S, S, S, S, E, E, E, E, E, E, N, N};
        Move move = new Move(initialPosition, directions);

        List<Point> actualPositions = gameService.applyMoves(move);

        List<Point> expectedPositions = new ArrayList<>() {{
            add(new Point(3,0));
            add(new Point(3,1));
            add(new Point(3,2));
            add(new Point(3,3));
            add(new Point(3,4));
            add(new Point(4,4));
            add(new Point(5,4));
            add(new Point(6,4));
            add(new Point(7,4));
            add(new Point(8,4));
            add(new Point(9,4));
            add(new Point(9,3));
            add(new Point(9,2));
        }};

        assertEquals(expectedPositions, actualPositions);
    }
}