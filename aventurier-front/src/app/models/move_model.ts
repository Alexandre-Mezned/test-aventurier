export class Position {
    public x: number;
    public y: number;
}

export class Move {
    public initialPosition?: Position;
    public directions?: String[];
}