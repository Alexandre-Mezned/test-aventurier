import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AventurierServiceService } from 'src/app/services/aventurier-service.service';
import { Move ,Position } from  'src/app/models/move_model';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  resultPositions:Position[] = [];
  move: Move = {};
  formMove:FormGroup;
  mapArray:String[] = [];
  

  constructor(
    private formBuilder: FormBuilder,
    private avService:AventurierServiceService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.showMap();
    
  }

  initForm(){
    this.formMove = this.formBuilder.group({
      initialPosition: this.formBuilder.group({
        x:['', Validators.required],
        y:['', Validators.required]
      }),
      directions:['', Validators.required]
    });
  }

  sendMoves(){    
    this.move.directions = this.formMove.get('directions').value.split('');
    this.move.initialPosition = this.formMove.get('initialPosition').value;

    this.avService.loadMoves(this.move).then(
      (data:any)=>{
        this.resultPositions = data;
      }
    );
  }

  showMap(){
    this.avService.loadMap().then(
      (data:any)=>{
        this.mapArray.push(data);
      }
    )
  }

}
