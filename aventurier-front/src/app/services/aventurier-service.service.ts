import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AventurierServiceService {

  MoveURL = 'http://127.0.0.1:8080/move/apply/';
  MapURL = 'http://127.0.0.1:8080/';

  constructor(
    private http:HttpClient,
  ) { }

  loadMap(){
    const promise = new Promise((resolve, reject) => {
      const apiURL = this.MapURL;
      this.http
        .get(apiURL)
        .toPromise()
        .then((result:any) => {
          resolve(result);
        },
          error => {
            reject(error);
          }
        );
    });

    return promise;
  }

  loadMoves(body){
    const promise = new Promise((resolve, reject) => {
      const apiURL = this.MoveURL;

      this.http
        .put(apiURL, body) 
        .toPromise()
        .then((result:any) => {
          resolve(result);
        },
          error => {
            reject(error);
          }
        );
    });

    return promise;
  }
  
}
