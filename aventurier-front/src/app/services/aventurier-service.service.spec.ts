import { TestBed } from '@angular/core/testing';

import { AventurierServiceService } from './aventurier-service.service';

describe('AventurierServiceService', () => {
  let service: AventurierServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AventurierServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
